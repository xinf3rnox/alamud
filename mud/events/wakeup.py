# -*- coding: utf-8 -*-
# Copyleft 2015 J&L, IUT d'Orléans
#==============================================================================

from .event import Event2
import mud.game
from .info  import InfoEvent

class WakeUpEvent(Event2):
    NAME = "wakeup"

    def get_event_templates(self):
        return self.actor.container().get_event_templates()

    def perform(self):
        if( self.actor.container().has_prop("reveillable")):
            self.buffer_clear()
            self.buffer_inform("wakeup.actor", object=self.actor.container())
            self.actor.send_result(self.buffer_get())
            self.actor.move_to(self.object[0])
        else:
            self.actor.send_result("vous etes déja reveillez")